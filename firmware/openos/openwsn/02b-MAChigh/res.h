#ifndef __RES_H
#define __RES_H

/**
\addtogroup MAChigh
\{
\addtogroup RES
\{
*/

#include "opentimers.h"

//----------------------------------------------------------------------------------------------
//#include "detas.h" //TODO DETAS REMOVE
//----------------------------------------------------------------------------------------------

//=========================== define ==========================================

//=========================== typedef =========================================

//=========================== module variables ================================

typedef struct {
   uint16_t        periodMaintenance;
   bool            busySendingKa;        // TRUE when busy sending a keep-alive
   bool            busySendingAdv;       // TRUE when busy sending an advertisement
   uint8_t         dsn;                  // current data sequence number
   uint8_t         MacMgtTaskCounter;    // counter to determine what management task to do


   //----------------------------------------------------------------------------------------------
   uint8_t         MacMgtTaskCounterDetas;   	// counter to determine the delay of REQ/RES sending
   bool            busySendingReqOrRes;       	// TRUE when busy sending a REQ/RES
   //----------------------------------------------------------------------------------------------


   opentimer_id_t  timerId;
} res_vars_t;

//=========================== prototypes ======================================

void    res_init();
bool    debugPrint_myDAGrank();
// from upper layer
error_t res_send(OpenQueueEntry_t *msg);
// from lower layer
void    task_resNotifSendDone();
void    task_resNotifReceive();



//----------------------------------------------------------------------------------------------
void 	sendREQ(); // sends a REQ command
void 	sendRES(); // sends a RES command in broadcast
void 	resendRES(open_addr_t* dest); // sends a RES command in unicast
void	res_setRESdelay(uint8_t delay);
//----------------------------------------------------------------------------------------------

/**
\}
\}
*/

#endif
